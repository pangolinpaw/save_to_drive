import os
import json
import datetime
import configparser
import logging
from pydrive import pydrive

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'
TEMP_DIRECTORY = f'{THIS_DIRECTORY}temp{os.sep}'
if not os.path.exists(TEMP_DIRECTORY):
    os.mkdir(TEMP_DIRECTORY)
HISTORY = f'{THIS_DIRECTORY}upload_history.json'

logging.basicConfig(
    filename=f'{THIS_DIRECTORY}save_to_drive.log',
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p',
    level=logging.INFO
)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.WARNING)


def list_files(folder, extensions=None):
    file_list = []
    all_files = os.listdir(folder)
    for name in all_files:
        if extensions is not None:
            for ext in extensions:
                if name.endswith(ext):
                    file_list.append(f'{folder}{os.sep}{name}')
        else:
            file_list.append(f'{folder}{os.sep}{name}')
    return file_list

def load_config():
    settings = {
        'source':None,
        'destination':None,
        'file_extensions':None
    }
    config_file = f'{THIS_DIRECTORY}save_to_drive.ini'
    if not os.path.exists(config_file):
        config = configparser.ConfigParser()
        config['FILES_AND_FOLDERS'] = {
            'source_directory': '',
            'destination_directory': 'SeaLens PEBLCAM Uploads',
            'file_extensions':'h264, mp4'
        }
        with open(config_file, 'w', encoding='utf-8') as f:
            config.write(f)

    config = configparser.ConfigParser()
    try:
        config.read(config_file)
        settings['destination'] = config['FILES_AND_FOLDERS']['destination_directory']
        settings['source'] = config['FILES_AND_FOLDERS']['source_directory']
        if settings['source'] == '':
            settings['source'] = THIS_DIRECTORY
        settings['file_extensions'] = [ext.strip() for ext in config['FILES_AND_FOLDERS']['file_extensions'].split(',')]
    except:
        os.remove(config_file)
        settings = load_config()
    return settings

def upload_history():
    if os.path.exists(HISTORY):
        with open(HISTORY, 'r', encoding='utf-8') as f:
            history = json.loads(f.read())
    else:
        history = {}
    return history

def save_history(history):
    with open(HISTORY, 'w', encoding='utf-8') as f:
        f.write(json.dumps(history, indent=2))


def main():
    settings = load_config()
    logger.info(f'Starting up with the following settings:\n\tSource directory = {settings["source"]}\n\tGoogle Drive directory = {settings["destination"]}\n\tTarget file extensions = {settings["file_extensions"]}')
    file_list = list_files(settings['source'], extensions=settings['file_extensions'])
    logger.debug(f'{len(file_list)} files found')
    history = upload_history()
    upload_counter = 0
    success_counter = 0
    skipped_counter = 0
    for filename in file_list:
        if filename.split(os.sep)[-1] not in history:
            upload_counter += 1
            if not filename.endswith('mp4'):
                upload_as = f'{TEMP_DIRECTORY}{filename.split(os.sep)[-1].split(".")[0]}.mp4'
                os.system(f'MP4Box -fps 30 -add {filename} {upload_as}')
                delete_file = True
            else:
                upload_as = filename
                delete_file = False
            logger.debug(f'Uploading {filename.split(os.sep)[-1]} to drive')
            try:
                file_id = pydrive.save_file(upload_as, settings['destination'])
                logger.debug(f'Uploaded with ID {file_id}')
                history[filename.split(os.sep)[-1]] = {'id':file_id, 'uploaded':datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}
                success_counter += 1
                if delete_file:
                    os.remove(upload_as)
            except Exception as e:
                logger.exception(f'Upload of {filename.split(os.sep)[-1]} failed with the following exception')
        else:
            skipped_counter += 1
            logger.debug(f'Skipped {filename.split(os.sep)[-1]}; previously uploaded on {history[filename.split(os.sep)[-1]]["uploaded"]}')
    save_history(history)
    logger.info(f'Run complete. Of {len(file_list)} files, upload was attempted for {upload_counter} ({success_counter} succeeded) and {skipped_counter} had been previously uploaded')

if __name__ == '__main__':
    main()
