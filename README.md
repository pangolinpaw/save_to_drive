# Save To Drive
## Introduction

## Installation
### Google Drive API
In order for the script to programatically access your Google Drive, you must first enable the Google Workspace API and generate a file of credentials. These steps are best carried out on a desktop, laptop or Raspberry Pi with monitor, keyboard, mouse etc.

#### Initial setup
1. Follow the instructions on https://developers.google.com/workspace/guides/create-project to create a new Goole CLoud Platform project and enable the API.
2. Once a project has been created, open the [Google Cloud Console](https://console.cloud.google.com/) and ensure that project is selected in the drop-down at the very top of the page.
3. Find and click the *APIs & Services* in the left pane
4. Use the search field to find the *Google Drive API*
5. Select this option and click *Enable*

#### API credentials
1. Click the *Create credentials* button from the enabled API's page
2. Follow the on-screen instructions and provide the relevant details
3. When promted for *scopes*, add the Google Drive API score with the description "See, edit, create and delete all your Google Drive files"
4. If required, record the app as a *Desktop application* and give it a name
5. **Download the credential information** using the Download button.
6. Rename this downloaded JSON file *credentials.json* and save it for later.


### 4G HAT
The following assumes you have a Raspberry Pi with the latest version of Raspbian installed and a working internet connection. Before you begin, **insert activated 4G SIM card into the HAT**.

1. Install minicom
```
sudo apt-get install minicom -y
```
2. Connect the 4G HAT to the pi via USB through the thimble (Raspberry Pi Zero) or via USB (other models of Raspberry Pi) as shown in [this image](https://www.waveshare.com/w/thumb.php?f=SIM7600G-H-4G-HAT-B-details-3.jpg&width=900). 
3. Ensrue the HAT is properly connected (no devices will be listed if not)
```
ls /dev/ttyUSB*
```
4. Open a port through minicom
```
sudo minicom -D /dev/ttyUSB2
```
5. Send the following command and wait for the module to restart
```
AT+CUSBPIDSWITCH=9011,1,1
```
6.  Exit minicom by pressing CTRL+A, then X. A USB0 card should now be detected
```
ifconfig
```
7. The IP address of the connection can be displayed with the following command
```
sudo dhclient -v usb0
```

### Software
On each pi **with a monitor connected and the desktop running**, make sure everything is up to date with

```
sudo apt-get update
sudo apt-get upgrade
```

Then run the following command to download and install the save_to_drive script:

```
git clone https://pangolinpaw@bitbucket.org/pangolinpaw/save_to_drive.git && cd save_to_drive && pip3 install -r requirements.txt
```

The *credentials.json* file created earlier must be shared with each Raspberry Pi that will be running the save_to_drive script. This is best done via SFTP using [WinSCP](https://winscp.net/eng/download.php) and the file should be saved to the `save_to_drive/pydrive` folder created by the command above.

Navigate to the `save_to_drive/pydrive` folder and run *pydrive.py*:

```
cd save_to_drive/pydrive
python pydrive.py
```

This will open the web browser and ask you to grant access to your Google Drive to the application. Follow the on-screen instructions until the authentication process is completed. If successful, a the terminal window running *pydrive.py* will display a list of all the folders on your Google Drive and a new `pydrive_test/testing.txt` file will appear in your Drive.

### Other dependencies
In order to wrap the captured h264 videos into a more compatible format, install gpac:

```
sudo apt-get install gpac
```

## Usage
The *save_to_drive.py* script is intended to run on a schedule, or to be triggered by another script that is running on a schedule. However, you may wish to run it manually the first time as this will genereate the configuration file *save_to_drive.ini* that gives you control over some running options.

By default, *save_to_drive.ini* will contain the following:
```
[FILES_AND_FOLDERS]
source_directory = 
destination_directory = SeaLens PEBLCAM Uploads
file_extensions = h264, mkv, mp4
```

- *source_directory* is the directory which contains the files to upload. If left blank (as is it is by default), the directory containing *save_to_drive.py* will be used.
- *destination_directory* is the name of the folder on the Google Drive to which the files are to be uploaded. This folder will be created if it does not exist.
- *file_extensions* is a comma-separated list of file extensions which should be included in the upload. Any other file types will be ignored.