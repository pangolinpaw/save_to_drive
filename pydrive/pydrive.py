import pickle
import os
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly',
          'https://www.googleapis.com/auth/drive.file']

def get_gdrive_service():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(f'{THIS_DIRECTORY}token.pickle'):
        with open(f'{THIS_DIRECTORY}token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                f'{THIS_DIRECTORY}credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(f'{THIS_DIRECTORY}token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    # return Google Drive API service
    return build('drive', 'v3', credentials=creds)

def upload_file(service, parent_folder, filename):
    file_metadata = {
        'name': filename.split(os.sep)[-1],
        'parents': [parent_folder]
    }
    media = MediaFileUpload(filename, resumable=True)
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    return file.get('id')

def create_folder(service, folder_name, parent_folder=None):
    folder_metadata = {
        'name': folder_name,
        'mimeType': 'application/vnd.google-apps.folder'
    }
    if parent_folder is not None:
        folder_metadata['parents'] = [parent_folder]
    folder = service.files().create(body=folder_metadata, fields="id").execute()
    return folder.get('id')

def drive_folders(service):
    response = service.files().list(q="mimeType='application/vnd.google-apps.folder'",
                                    spaces='drive',
                                    fields='files(id, name)').execute()
    folders = {}
    for folder in response.get('files', []):
        folders[folder.get('name')] = folder.get('id')
    return folders

def delete_folder(service, folder_id):
    service.files().delete(fileId=folder_id).execute()

def root_folder(service):
    return service.files().insert(body = None).execute()

def list_folders():
    service = get_gdrive_service()
    return drive_folders(service)

def save_file(filename, folder):
    service = get_gdrive_service()
    folders = drive_folders(service)
    if folder in folders:
        folder_id = folders[folder]
    else:
        folder_id = create_folder(service, folder)
    return upload_file(service, folder_id, filename)


if __name__ == '__main__':
    for folder in list_folders():
        print(folder)
    save_file('test.txt', 'pydrive_test')
